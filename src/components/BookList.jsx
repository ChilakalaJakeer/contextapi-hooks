import React, { useContext } from "react";
import { ThemeContext } from "../context/ThemeContextProvider";
import { BookContext } from "../context/BookContextProvider";
import AddBook from "./AddBook";

const BookList = () => {
  const themeContext = useContext(ThemeContext);
  const bookContext = useContext(BookContext);

  const { isLightTheme, lightTheme, darkTheme } = themeContext;
  const theme = isLightTheme ? lightTheme : darkTheme;

  const { books, dispatch } = bookContext;
  console.log(bookContext);

  return (
    <div
      className="book-list"
      style={{ color: theme.textColor, background: theme.bg }}
    >
      <ul>
        {books.map(book => (
          <li
            style={{ background: theme.ui }}
            key={book.id}
            onClick={() => dispatch({ type: "REMOVE_BOOK", id: book.id })}
          >
            <div className="title">{book.title}</div>
            <br />
            <div className="author">{book.author}</div>
          </li>
        ))}
      </ul>
      <AddBook />
    </div>
  );
};

export default BookList;
