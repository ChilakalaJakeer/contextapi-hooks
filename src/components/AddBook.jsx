import React, { useState, useContext } from "react";
import { BookContext } from "../context/BookContextProvider";

const AddBook = props => {
  const [title, setTitle] = useState("");
  const [author, setAuthor] = useState("");

  const bookContext = useContext(BookContext);

  const { dispatch } = bookContext;
  const onSumbit = e => {
    e.preventDefault();
    title && author && dispatch({ type: "ADD_BOOK", book: { title, author } });
    setTitle("");
    setAuthor("");
  };
  return (
    <form onSubmit={onSumbit}>
      Add Book
      <br />
      <input
        type="text"
        value={title}
        placeholder="Book title"
        onChange={e => setTitle(e.target.value)}
      />
      <br />
      <br />
      <input
        type="text"
        value={author}
        placeholder="Book author"
        onChange={e => setAuthor(e.target.value)}
      />
      <input type="submit" value="Add book" />
    </form>
  );
};

export default AddBook;
