// Allahhamdulillah
import React, { Component } from "react";
import { ThemeContext } from "../context/ThemeContextProvider";

class ToggleTheme extends Component {
  static contextType = ThemeContext;

  render() {
    const { toggleTheme, isLightTheme } = this.context;
    return (
      <button
        onClick={toggleTheme}
        className="toggle-theme"
        style={{ background: isLightTheme ? "black" : "white" }}
      >
        {isLightTheme ? "🌙" : "🌅"}
      </button>
    );
  }
}

export default ToggleTheme;
