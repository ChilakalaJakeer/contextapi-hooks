// Allahmdulillah
import React, { Component } from "react";
import { ThemeContext } from "../context/ThemeContextProvider";
import ToggleTheme from "./ToggleTheme";
import { AuthContext } from "../context/AuthContextProvider";

class Navbar extends Component {
  render() {
    return (
      <AuthContext.Consumer>
        {authContext => (
          <ThemeContext.Consumer>
            {themeContext => {
              console.log(themeContext);
              const { isAuthenticated, toggleAuth } = authContext;
              const { isLightTheme, lightTheme, darkTheme } = themeContext;
              const theme = isLightTheme ? lightTheme : darkTheme;
              return (
                <nav style={{ color: theme.textColor, background: theme.ui }}>
                  <h1>Context App</h1>
                  <ul>
                    <li>Home</li>
                    <li>About</li>
                    <li>Contact</li>
                  </ul>
                  <ToggleTheme />
                  <button className="toggle-auth" onClick={toggleAuth}>
                    {isAuthenticated ? "Logged in" : "Logged out"}
                  </button>
                </nav>
              );
            }}
          </ThemeContext.Consumer>
        )}
      </AuthContext.Consumer>
    );
  }
}

export default Navbar;
