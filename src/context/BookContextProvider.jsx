import React, { useReducer, createContext, useEffect } from "react";
import uuid from "uuid/v1";
import booksReducer from "../reducers/bookReducer";

export const BookContext = createContext();

const BookContextProvider = props => {
  const initialData = [
    { title: "title1", author: "author1", id: uuid() },
    { title: "title2", author: "author2", id: uuid() },
    { title: "title3", author: "author3", id: uuid() }
  ];
  const [books, dispatch] = useReducer(booksReducer, initialData, () => {
    const localData = localStorage.getItem("books");
    return localData ? JSON.parse(localData) : initialData;
  });

  useEffect(() => {
    localStorage.setItem("books", JSON.stringify(books));
  }, [books]);
  return (
    <BookContext.Provider value={{ books, dispatch }}>
      {props.children}
    </BookContext.Provider>
  );
};

export default BookContextProvider;
