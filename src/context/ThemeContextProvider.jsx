import React, { Component } from "react";

export const ThemeContext = React.createContext();

class ThemeContextProvider extends Component {
  state = {
    isLightTheme: true,
    lightTheme: {
      textColor: "#000",
      ui: "#ff99ff",
      bg: "#ffc7ff"
    },
    darkTheme: {
      textColor: "#fff",
      ui: "#394538",
      bg: "#777"
    }
  };

  toggleTheme = () => {
    this.setState({ isLightTheme: !this.state.isLightTheme });
  };
  render() {
    return (
      <ThemeContext.Provider
        value={{ ...this.state, toggleTheme: this.toggleTheme }}
      >
        {this.props.children}
      </ThemeContext.Provider>
    );
  }
}

export default ThemeContextProvider;
